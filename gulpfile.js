var gulp = require('gulp');
var babelify = require('babelify');
var source = require('vinyl-source-stream');
var browserify = require('browserify');
var webserver = require('gulp-webserver');

gulp.task('default', ['build.web']);

gulp.task('build.web', function () {

    browserify({ entries: 'src/interface/app.js', debug: true })
        .transform(babelify)
        .bundle()
        .pipe(source('app.js'))
        .pipe(gulp.dest('dist'));
        
    gulp.src('src/interface/*.html')
        .pipe(gulp.dest('dist'));

    gulp.src('src/interface/*.css')
        .pipe(gulp.dest('dist'));

    //gulp.src('src/interface/assets/**/*.*').pipe(gulp.dest('dist/assets'));
});

gulp.task('run.web', ['build.web'], function() {
  gulp.src('dist')
    .pipe(webserver({
      fallback: 'index.html',
      open: true
    }));
});