import Event from './event.js';

/* 
In an encounter, there are two parties. A left one, and a right one. The encounter ends when one of the 
parties no longer has any alive characters.
*/
export default class Encounter extends Event {

    constructor(left, right) {
        super();
        this.left = left;
        this.right = right;
    }

    start(fn) {
        this.left.targetParty(this.right);
        this.right.targetParty(this.left);
        super.start(fn);
    }

    finish() {
        this.left.targetParty(null);
        this.right.targetParty(null);
        this.left = null;
        this.right = null;
    }

    nextTurn() {
        if (this.current) this.current.characters.forEach(c => c.finishTurn());
        this.current = this.current == this.left ? this.right : this.left;
        this.current.characters.forEach(c => c.startTurn())
    }

    get isComplete() {
        return this.winner != null;
    }

    get winner() {
        if (this.left.characters.length === 0)
            return this.right;

        else if (this.right.characters.length === 0)
            return this.left;

        return null;
    }

}