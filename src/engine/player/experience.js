const DEFAULT_TIERS = [ 10, 20, 30 ]

export default class Experience {

    constructor(tiers) {
        this.level = 1;
        this.tiers = tiers || DEFAULT_TIERS;
        this.experience = 0;
    }

    gainExperience(exp) {
        if (this.level >= 4) return;
        this.experience += exp;

        var tier = this.tiers[this.level - 1];
        if (this.experience < tier) return;

        this.level++;
        if (this.level == 4)
            this.experience = 0;
        else
            this.experience -= tier;
    }

    loseExperience(exp) {
        if (this.experience <= 0) return;
        this.experience -= exp;
        if (this.experience < 0) this.experience = 0;
    }

    setLevel(lvl) {
        if (this.level > 4) return;
        this.level = lvl;
        this.experience = 0;
    }

}
    