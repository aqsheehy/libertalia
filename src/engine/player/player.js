import Party from './party.js';
import Backpack from './backpack.js';
import Purse from './purse.js';

export default class Player {

    constructor(game) {
        this.game = game;
        this.team = new Party();
        this.backpack = new Backpack();
        this.purse = new Purse();
    }
}