const MARKED_MODIFIER = 2;

export default class Character {

    constructor(attributes) {
        this.attributes = attributes || { damage: 0, health: 0 };
        if (!this.attributes.bleed) this.attributes.bleed = 0;
        if (!this.attributes.poison) this.attributes.poison = 0;
    }

    get isActive(){
        return this.attributes.stunned;
    }

    joinParty(party) {
        if (this.party) this.party.removeCharacter(this);
        party.addCharacter(this);
        this.party = party;
    }

    damage(pts, from) {
        if (this.attributes.marked) pts *= MARKED_MODIFIER;
        this.attributes.health -= pts;
        if (this.attributes.health <= 0) {
            this.attributes.health = 0;
            this.die();
        }
    }

    stun(){
        this.attributes.stunned = true;
    }

    heal(pts, from) {
        if (this.attributes.dead) return;
        this.health += pts;
    }

    marked(from) {
        this.attributes.marked = true;
    }

    bleed(pts, from) {
        this.attributes.bleed += pts;
    }

    poison(pts, from) {
        this.attributes.poison += pts;
    }

    cure(from) {
        this.attributes.marked = false;
        this.attributes.bleed = 0;
        this.attributes.poison = 0;
    }

    die() {
        if (this.attributes.dead) return;
        this.attributes.dead = true;
        if (this.party) this.party.bury(this);
    }

    resurrect() {
        if (!this.attributes.dead) return;
        this.attributes.dead = false;
        if (this.party) this.party.resurrect(this);
    }

    startTurn() { }

    finishTurn() {
        this.damage(
            this.attributes.bleed + 
            this.attributes.poison);
            
        this.attributes.stunned = false;
    }

    startEvent(){ }

    finishEvent(){
        this.attributes.marked = false;
        this.attributes.bleed = 0;
    }
}