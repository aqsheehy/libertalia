const MAX_BACKPACK_SIZE = 6;

export default class Backpack {

    constructor(maxSize) {
        this.maxSize = maxSize || MAX_BACKPACK_SIZE;
        this.items = [];
    }

    receiveItem(item) {
        if (this.items.length < this.maxSize)
            this.items.push(item);
    }

    dropItem(item){
        var index = this.items.indexOf(item);
        if (index === -1) return;
        this.items.splice(index);
    }

    moveItem(item, target) {
        if (target > (this.maxSize - 1)) return;
        var index = this.items.indexOf(item);
        if (index === -1) return;
        this.items[index] = this.items[target];
        this.items[target] = item;
    }

}