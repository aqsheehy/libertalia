const MAX_PARTY_SIZE = 4;

export default class Party {

    constructor(maxLength) {
        this.maxLength = maxLength || MAX_PARTY_SIZE;
        this.characters = [];
        this.graveyard = [];
    }

    get isFull() {
        return this.characters.length >= this.maxLength;
    }

    targetParty(enemy) {
        this.enemy = enemy;
    }

    addCharacter(character) {
        if (!this.isFull) this.characters.push(character);
    }

    removeCharacter(character){
        var index = this.characters.indexOf(character);
        if (index === -1) return;
        return this.characters.splice(index);
    }

    shiftForward(character) {
        var index = this.characters.indexOf(character);
        if (index === -1) return;
        if (index === 0) return;
        var infront = this.characters[index - 1];
        this.characters[index - 1] = character;
        this.characters[index] = infront;
    }

    shiftBackward(character) {
        var index = this.characters.indexOf(character);
        if (index === -1) return;
        if (index === (this.characters.length - 1)) return;
        var behind = this.characters[index + 1];
        this.characters[index + 1] = character;
        this.characters[index] = behind;
    }

    bury(character) {
        this.removeCharacter(character);
        this.graveyard.push(character);
    }

    resurrect(character) {
        if (this.isFull) return;
        var index = this.graveyard.indexOf(character);
        if (index === -1) return;
        this.graveyard.splice(index);
        this.characters.push(character);
    }

    resurrectOne() {
        if (this.graveyard.length === 0) return;
        if (this.isFull) return;
        var characters = this.graveyard.splice(0);
        this.characters.push(characters[0]);
    }

    traceForward(origin, length){
        var results = [];
        var index = this.characters.indexOf(origin);
        if (index === -1) return results;
        for (var i = 1; i < length + 1; i++){
            var result = this.characters[index - i];
            if (!result) return results;
            results.push(result);
        }
        return results;
    }

    traceBackward(origin, length){
        var results = [];
        var index = this.characters.indexOf(origin);
        if (index === -1) return results;
        for (var i = 1; i < length + 1; i++){
            var result = this.characters[index + i];
            if (!result) return results;
            results.push(result);
        }
        return results;
    }
}