export default class Purse {

    constructor() {
        this.gold = 0;
    }

    receiveGold(gold) {
        this.gold += gold || 1;
        if (this.gold < 0) this.gold = 0;
    }

    dropGold(gold) {
        this.gold -= gold || 1;
        if (this.gold < 0) this.gold = 0;
    }    
}