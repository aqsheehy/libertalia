import Slot from './slot.js';

export default class Map {

    constructor(width, height) {
        this.rows = [];
        this.width = width;
        this.height = height;
        for (var y = 0; y < height; y++) {
            this.rows[y] = [];
            for (var x = 0; x < width; x++) {
                this.rows[y][x] = new Slot(null, x, y);
            }
        }
    }

    fill(fn) {
        for (var y = 0; y < this.height; y++){
            for (var x = 0; x < this.width; x++){
                this.rows[y][x].event = fn(x, y);
            }
        }
    }

    setEvent(x, y, event) {
        this.rows[y][x].event = event;
    }

    getEvent(x, y) {
        return this.rows[y][x].event;
    }

}