export default class Slot {
    constructor(event, x, y) {
        this.event = event;
        this.position = { x: x, y: y };
    }   
}