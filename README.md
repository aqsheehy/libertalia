Event types:

- Flat reward
- Reward after encounter
- Pay for reward

Rewards: Everything listed can also be a negative (trap)..

- Money, Heroes, Items, Health, Experience
- Trade (swap the X from opponent for Y from self)
- Raid (next enemy encounter has additional enemies)
- Thief (next reward is given to you)

Heroes:

- Tank: 3/20
1. Shield Up: _/+5
2. Counterattack (Passive): _/-1 to any attackers
3. Taunt (Passive): The only valid target is this

- Assasin: 10/10

- Healer: 2/10

Combat effects types:

- // Heal
- // Stun
- // Direct Damage
- // Explosive Damage
- // Piercing Damage
- // DoT (Round) [Bleed]
- // DoT (Game) [Poison]
- // Mind control
- // Resurrect
- // Summon
- // Amplify damage (Marked)
- // Decurse (Cure)
- // Move/Pull Forward
- // Move/Push Back    
- Escape (Exit the encounter leaving it unlocked, move back 1 space)
