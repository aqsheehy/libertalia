import chai, { expect } from 'chai';
import Experience from '../src/engine/player/experience.js';

describe('Experience', () => {

  it('should default to level 1', () => {
    var exp = new Experience([10, 20, 30]);
    expect(exp.level).to.equal(1);
  });

  it('should handle tallying experience', () => {
    var exp = new Experience([10, 20, 30]);
    exp.gainExperience(8);
    expect(exp.experience).to.equal(8);
    expect(exp.level).to.equal(1);
  });

  it('should be able to level up 3 times', () => {
    var exp = new Experience([10, 20, 30]);
    exp.gainExperience(10);
    expect(exp.experience).to.equal(0);
    expect(exp.level).to.equal(2);

    exp.gainExperience(20);
    expect(exp.experience).to.equal(0);
    expect(exp.level).to.equal(3);

    exp.gainExperience(30);
    expect(exp.experience).to.equal(0);
    expect(exp.level).to.equal(4);
  });

  it('should be able to carry experience into the next level', () => {
    var exp = new Experience([10, 20, 30]);
    exp.gainExperience(13);
    expect(exp.experience).to.equal(3);
    expect(exp.level).to.equal(2);
  });

  it('should be ignore carried experience on max level', () => {
    var exp = new Experience([10, 20, 30]);
    exp.setLevel(3);
    exp.gainExperience(35);
    expect(exp.experience).to.equal(0);
    expect(exp.level).to.equal(4);
  });

  it('should be able to lose experience', () => {
    var exp = new Experience([10, 20, 30]);
    exp.gainExperience(8);
    exp.loseExperience(5);
    expect(exp.experience).to.equal(3);
  });

  it('should not be able to lose a level through loss of experience', () => {
    var exp = new Experience([10, 20, 30]);
    exp.gainExperience(11);
    exp.loseExperience(10);
    expect(exp.experience).to.equal(0);
    expect(exp.level).to.equal(2);
  });

});