import chai, { expect } from 'chai';
import Party from '../src/engine/player/party.js';
import Character from '../src/engine/player/character.js';

describe('Party', () => {
  it('should default to a max length of 4', () => {
    var party = new Party();
    expect(party.maxLength).to.equal(4);
  });

  it('should validate its maxLength', () => {
    var party = new Party(1);
    expect(party.isFull).to.equal(false);
    party.addCharacter(new Character())
    expect(party.isFull).to.equal(true);
  });

  it('should enforce its maxLength', () => {
    var party = new Party(1);
    expect(party.isFull).to.equal(false);
    party.addCharacter(new Character());
    expect(party.isFull).to.equal(true);
    party.addCharacter(new Character());
    expect(party.characters.length).to.equal(1);
  });

  it('should support shifting a character forward', () => {
    var party = new Party();

    var a = new Character();
    var b = new Character();
    var c = new Character();
    var d = new Character();
    party.addCharacter(a);
    party.addCharacter(b);
    party.addCharacter(c);
    party.addCharacter(d);

    party.shiftForward(c);
    expect(party.characters[0]).to.equal(a);
    expect(party.characters[1]).to.equal(c);
    expect(party.characters[2]).to.equal(b);
    expect(party.characters[3]).to.equal(d);
  });

  it('should remain unchanged when shifting a character forward at the front', () => {
    var party = new Party();
    
    var a = new Character();
    var b = new Character();
    var c = new Character();
    var d = new Character();
    party.addCharacter(a);
    party.addCharacter(b);
    party.addCharacter(c);
    party.addCharacter(d);

    party.shiftForward(a);
    expect(party.characters[0]).to.equal(a);
    expect(party.characters[1]).to.equal(b);
    expect(party.characters[2]).to.equal(c);
    expect(party.characters[3]).to.equal(d);
  });

  it('should support shifting a character backward', () => {
    var party = new Party();

    var a = new Character();
    var b = new Character();
    var c = new Character();
    var d = new Character();
    party.addCharacter(a);
    party.addCharacter(b);
    party.addCharacter(c);
    party.addCharacter(d);

    party.shiftBackward(c);
    expect(party.characters[0]).to.equal(a);
    expect(party.characters[1]).to.equal(b);
    expect(party.characters[2]).to.equal(d);
    expect(party.characters[3]).to.equal(c);
  });

  it('should remain unchanged when shifting a character forward at the front', () => {
    var party = new Party();

    var a = new Character();
    var b = new Character();
    var c = new Character();
    var d = new Character();
    party.addCharacter(a);
    party.addCharacter(b);
    party.addCharacter(c);
    party.addCharacter(d);

    party.shiftBackward(d);
    expect(party.characters[0]).to.equal(a);
    expect(party.characters[1]).to.equal(b);
    expect(party.characters[2]).to.equal(c);
    expect(party.characters[3]).to.equal(d);
  });

  it('should support sending characters to the graveyard', () => {
    var party = new Party();
    var a = new Character();
    party.addCharacter(a);
    party.bury(a);
    expect(party.graveyard.length).to.equal(1);
    expect(party.characters.length).to.equal(0);
  });

  it('should support resurrecting characters from the graveyard', () => {
    var party = new Party();
    var a = new Character();
    party.addCharacter(a);
    party.bury(a);
    party.resurrectOne();
    expect(party.graveyard.length).to.equal(0);
    expect(party.characters.length).to.equal(1);
  });

  it('should not resurrecting characters from the graveyard if the party is full', () => {
    var party = new Party(2);

    var a = new Character();
    party.addCharacter(a);
    party.bury(a);

    party.addCharacter(new Character());
    party.addCharacter(new Character());
    party.resurrectOne();

    expect(party.graveyard.length).to.equal(1);
    expect(party.characters.length).to.equal(2);
  });

  it('should be able to trace forward through characters in a line from an origin for a length', () => {
    var party = new Party();

    var a = new Character();
    var b = new Character();
    var c = new Character();
    var d = new Character();
    party.addCharacter(a);
    party.addCharacter(b);
    party.addCharacter(c);
    party.addCharacter(d);

    var traced = party.traceForward(d, 2);
    expect(traced.length).to.equal(2);
    expect(traced[0]).to.equal(c);
    expect(traced[1]).to.equal(b);
  });

  it('should be able to trace forward even when there arent the characters to meet the length', () => {
    var party = new Party();

    var a = new Character();
    var b = new Character();
    var c = new Character();
    var d = new Character();
    party.addCharacter(a);
    party.addCharacter(b);
    party.addCharacter(c);
    party.addCharacter(d);

    var traced = party.traceForward(b, 2);
    expect(traced.length).to.equal(1);
    expect(traced[0]).to.equal(a);
  });

  it('should be able to trace backward through characters in a line from an origin for a length', () => {
    var party = new Party();

    var a = new Character();
    var b = new Character();
    var c = new Character();
    var d = new Character();
    party.addCharacter(a);
    party.addCharacter(b);
    party.addCharacter(c);
    party.addCharacter(d);

    var traced = party.traceBackward(a, 2);
    expect(traced.length).to.equal(2);
    expect(traced[0]).to.equal(b);
    expect(traced[1]).to.equal(c);
  });

  it('should be able to trace backward even when there arent the characters to meet the length', () => {
    var party = new Party();

    var a = new Character();
    var b = new Character();
    var c = new Character();
    var d = new Character();
    party.addCharacter(a);
    party.addCharacter(b);
    party.addCharacter(c);
    party.addCharacter(d);

    var traced = party.traceBackward(c, 2);
    expect(traced.length).to.equal(1);
    expect(traced[0]).to.equal(d);
  });
});