import chai, { expect } from 'chai';
import Purse from '../src/engine/player/purse.js';

describe('Purse', () => {

  it('should be able to receive gold', () => {
    var purse = new Purse();
    purse.receiveGold(10);
    expect(purse.gold).to.equal(10);
  });

  it('should be able to drop gold', () => {
    var purse = new Purse();
    purse.receiveGold(10);
    purse.dropGold(8);
    expect(purse.gold).to.equal(2);
  });

  it('should not be able to drop gold into the negative', () => {
    var purse = new Purse();
    purse.dropGold(10);
    expect(purse.gold).to.equal(0);
  });

});