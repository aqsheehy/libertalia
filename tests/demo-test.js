import chai, { expect } from 'chai';
import GameEngine from '../src/engine/game.js';

describe('Demonstration test', () => {
  it('should work', () => {
      var game = new GameEngine();
      expect(game.test()).to.equal('It works yo!');
  });
});