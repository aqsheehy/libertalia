import chai, { expect } from 'chai';
import Backpack from '../src/engine/player/backpack.js';

describe('Backpack', () => {

  it('should be able to receive items', () => {
    var backpack = new Backpack();
    backpack.receiveItem({});
    expect(backpack.items.length).to.equal(1);
  });

  it('should enforce a backpack maximum length', () => {
    var backpack = new Backpack(2);
    backpack.receiveItem({});
    backpack.receiveItem({});
    expect(backpack.items.length).to.equal(2);
    backpack.receiveItem({});
    expect(backpack.items.length).to.equal(2);
  });

  it('should be able to drop items from a backpack', () => {
    var backpack = new Backpack(2);
    var item = {};
    backpack.receiveItem({});
    backpack.receiveItem(item);
    expect(backpack.items.length).to.equal(2);
    backpack.dropItem(item);
    expect(backpack.items.length).to.equal(1);
  });

  it('should be able to move items to specific indexes', () => {
    var backpack = new Backpack(6);
    var item = {};
    backpack.receiveItem(item);
    backpack.moveItem(item, 4);
    expect(backpack.items[0]).to.equal(undefined);
    expect(backpack.items[4]).to.equal(item);
  });

  it('should be able to move items to filled indexes, swapping them', () => {
    var backpack = new Backpack(6);
    var itemA = {};
    var itemB = {};
    backpack.receiveItem(itemA);
    backpack.receiveItem(itemB);
    backpack.moveItem(itemA, 1);
    expect(backpack.items[0]).to.equal(itemB);
    expect(backpack.items[1]).to.equal(itemA);
  });

  it('should be able to move items to the same index if the need arises', () => {
    var backpack = new Backpack(6);
    var itemA = {};
    backpack.receiveItem(itemA);
    backpack.moveItem(itemA, 0);
    expect(backpack.items[0]).to.equal(itemA);
  });

});