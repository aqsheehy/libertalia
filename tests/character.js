import chai, { expect } from 'chai';
import Character from '../src/engine/player/character.js';
import Party from '../src/engine/player/party.js';

describe('Character', () => {

  it('should be able to take damage', () => {
    var char = new Character({ damage: 0, health: 10 });
    char.damage(5, null);
    expect(char.attributes.health).to.equal(5);
    expect(char.attributes.dead).to.equal(undefined);
  });

  it('should be able to die', () => {
    var char = new Character({ damage: 0, health: 10 });
    char.damage(10, null);
    expect(char.attributes.dead).to.equal(true);
  });

  it('should be sent to the party graveyard on death', () => {
    var char = new Character({ damage: 0, health: 10 });
    var party = new Party();
    char.joinParty(party);
    char.damage(10, null);
    expect(party.graveyard.length).to.equal(1);
    expect(party.characters.length).to.equal(0);
  });

  it('should take bleed and poison damage when the turn finishes', () => {
    var char = new Character({ damage: 0, health: 10 });
    char.poison(3);
    char.bleed(6);
    char.finishTurn();
    expect(char.attributes.health).to.equal(1);
  });

  it('should be able to die from bleed / poison damage', () => {
    var char = new Character({ damage: 0, health: 10 });
    char.poison(4);
    char.bleed(6);
    char.finishTurn();
    expect(char.attributes.health).to.equal(0);
    expect(char.attributes.dead).to.equal(true);
  });

  it('should be able to move from one party to another', () => {
    var char = new Character({ damage: 0, health: 10 });

    var origin = new Party();
    char.joinParty(origin);
    expect(origin.characters.length).to.equal(1);

    var destination = new Party();
    char.joinParty(destination);
    expect(destination.characters.length).to.equal(1);
    expect(origin.characters.length).to.equal(0);

  });

  it('should be able to be stunned', () => {
    var char = new Character();
    char.stun();
    expect(char.attributes.stunned).to.equal(true);
  });

  it('should be no longer be stunned after a turn ends', () => {
    var char = new Character();
    char.stun();
    char.finishTurn();
    expect(char.attributes.stunned).to.equal(false);
  });

});