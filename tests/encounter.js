import chai, { expect } from 'chai';
import Character from '../src/engine/player/character.js';
import Party from '../src/engine/player/party.js';
import Encounter from '../src/engine/events/encounter.js';

describe('Encounter', () => {

  it('should pin the parties against eachother on start', () => {
    var left = new Party();
    var right = new Party();
    var encounter = new Encounter(left, right);
    encounter.start();
    expect(left.enemy).to.equal(right);
    expect(right.enemy).to.equal(left);
  });

  it('should detarget both parties on finish', () => {
    var left = new Party();
    var right = new Party();
    var encounter = new Encounter(left, right);
    encounter.start();
    encounter.finish();
    expect(left.enemy).to.equal(null);
    expect(right.enemy).to.equal(null);
  });

  it('should be able to increment the turns by a current pointer', () => {
    var left = new Party();
    var right = new Party();
    var encounter = new Encounter(left, right);
    encounter.start();
    encounter.nextTurn();
    expect(encounter.current).to.equal(left);
    encounter.nextTurn();
    expect(encounter.current).to.equal(right);
  });

  it('should be able to determine the winner and completion', () => {
    var left = new Party();
    new Character().joinParty(left);
    var right = new Party();
    new Character().joinParty(right);

    var encounter = new Encounter(left, right);
    expect(encounter.winner).to.equal(null);

    left.characters[0].die();
    expect(left.characters.length).to.equal(0);
    expect(encounter.winner).to.equal(right);
    expect(encounter.isComplete).to.equal(true);

    left.graveyard[0].resurrect();
    expect(left.characters.length).to.equal(1);
    expect(encounter.winner).to.equal(null);
    expect(encounter.isComplete).to.equal(false);

    right.characters[0].die();
    expect(right.characters.length).to.equal(0);
    expect(encounter.winner).to.equal(left);
    expect(encounter.isComplete).to.equal(true);
  });

});