import chai, { expect } from 'chai';
import Map from '../src/engine/map.js';

describe('Map', () => {
  it('should init slots correctly based on the given dimensions', () => {
    var map = new Map(3, 2);
    expect(map.width).to.equal(3);
    expect(map.height).to.equal(2);
    expect(map.rows.length).to.equal(map.height);
    expect(map.rows[0].length).to.equal(map.width);
    expect(map.rows[1].length).to.equal(map.width);
  });

  it('should support events overwriting', () => {
    var map = new Map(3, 2);
    map.setEvent(1, 0, { test: 'hi' });
    expect(map.rows[0][1].event.test).to.equal('hi');
  });
});